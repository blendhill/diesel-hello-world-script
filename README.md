## Simple 'Hello World' script written in Diesel

```
enableCulture en_EN
```

Defines **en_EN** as the culture supported in the document

```
applyPageSettings default
```

Marks the beginning of a sequence of pages sharing the same page settings. In this case, we use some default settings.

```
TextFragment tf = default with { text = "Hello World", culture = en_EN }
```

Defines a text fragment with **Hello World** as the contained text and with culture **en_EN**.

```
ParagraphStyle ps = default with { fontSize = 14, textColor = navy }
```

Defines a paragraph style, borrowing settings from the *default* entity and setting font size to **14** and text color to **navy**.

```
add Paragraph -> default with { textFragment = tf, style = ps }
```

Finally, add the paragraph to the template layout indicating the text fragment to use and the style to apply.